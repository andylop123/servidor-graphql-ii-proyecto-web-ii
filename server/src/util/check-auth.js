const jwt = require('jsonwebtoken');
const { SECRETE_KEY } = require('../config');

const checkToken = (req,res) => {
    const bearerHeader = req.headers['authorization'];
        if (bearerHeader) {
            const bearer = bearerHeader.split(' ');
            const token = bearer[1];
            if (token) {
                try {
                    const user = jwt.verify(token, SECRETE_KEY);
                    if (user) {
                        return;
                    }
                } catch (err) {
                    res.status(404);
                    res.json({ error: 'invalid token' })
                }


            }

        } else {
            res.status(401);
            res.send({
                error: "The request must have a token "
            });
        }
}

module.exports = {checkToken};