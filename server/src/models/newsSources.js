const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categories = mongoose.model('categories');
const user = mongoose.model('user');

//news source model
const newsSource = new Schema({
    url: String,
    name: String,
    category_id: { type: Schema.ObjectId, ref: "categories"},
    user_id: { type: Schema.ObjectId, ref: "user" }
});

module.exports = mongoose.model('newsSources', newsSource);