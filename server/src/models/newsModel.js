const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require("../Models/userModel");
const Category = require("../Models/categoryModel");
const newsSource = require("../Models/newsSources");


//news model
const news = new Schema({
    title: String,
    content: String,
    link: String,
    tagName: String,
    pubDate: String,
    user_id: { type: Schema.ObjectId, ref: User },
    category_id: { type: Schema.ObjectId, ref: Category },
    newsSource_id: { type: Schema.ObjectId, ref: newsSource }
});

module.exports = mongoose.model('news', news);