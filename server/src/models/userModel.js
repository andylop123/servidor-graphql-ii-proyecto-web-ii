const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//user model
const user = new Schema({
    _id: String,
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    phone : String
});

module.exports = mongoose.model('user', user);