const { gql } = require('apollo-server');

const typeDefs = gql`
type Query {
    hello:String,
    news(User_id:ID!):[News],
    newsSearch(text: String!,User_id:ID!):[News],
    newsTag(text: String!,User_id:ID!):[News],
}


type News {
    _id: ID!
    title: String!,
    content: String!,
    link: String!,
    tagName: String!,
    pubDate: String!,
    user_id: User!,
    category_id: Category!,
    newsSource_id: NewsSource!
}

type User{
    _id: ID!,
    firstName: String!,
    lastName: String!,
    email: String!,
    password: String!,
    phone : String!
}


type Category{
    _id: ID!,
    name: String!,
    user_id: User!
} 

type NewsSource{
    _id: ID!,
    url: String!,
    name: String!,
    category_id:Category!,
    user_id: User!
}
`;

module.exports = { typeDefs }