// const Posts = require('../models/postModel');

const mongoose = require('mongoose');
const News = require('../models/newsModel');

const resolvers = {
    Query: {

        /**
         * This method returns the news of a user by id 
         * @returns the list of the news 
         */
        hello: () => "Hello World",
        news: async (_, { User_id }) => {
            try {
                const news = await News.find({"user_id":User_id}).sort({ _id: -1 }).limit(50);
                if(news){
                    return news;

                }
            } catch (err) {
                throw new Error(err);
            }

        },
        /**
         * This method returns the news of a user by id and text in content or tittle
         * @returns the list of the news 
         */
        newsSearch: async (_, { text,User_id  }) => {
            try {
            const news = await News.find({$or:[{"title":new RegExp(text, 'i')}, {"content":new RegExp(text, 'i')}],"user_id":User_id}).sort({ _id: -1 }).limit(50);
                if(news){
                    return news;
                    
                }
            } catch (err) {
                throw new Error(err);
            }

        },
        /**
         * This method returns the news of a user by id and tag name 
         * @returns the list of the news 
         */
        newsTag: async (_, { text,User_id  }) => {
            try {
            const news = await News.find({"tagName":text,"user_id":User_id}).sort({ _id: -1 }).limit(50);
                if(news){
                    return news;

                }
            } catch (err) {
                throw new Error(err);
            }

        }

    },
    // Mutation: {

    // }
}



module.exports = { resolvers };