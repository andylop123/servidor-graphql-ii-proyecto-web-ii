const { ApolloServer, gql } = require('apollo-server');
const express = require('express');
const morgan = require('morgan');
const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');

const { MONGODB, SECRETE_KEY } = require('./config.js');

const { typeDefs } = require("./schema/typeDefs");
const { resolvers } = require("./schema/resolvers");
const { checkToken } = require("./util/check-auth");
//Conection with the database
mongoose.connect(MONGODB, { useNewUrlParser: true })
    .then(() => {
        console.log('Connect to DB')
    })






const server = new ApolloServer({
    // These will be defined for both new or existing servers
    context: ({ req, res }) => {
    checkToken(req, res)
    },
    typeDefs,
    resolvers

});
const app = express();
//middlewares
app.use(morgan('dev'));
//body -parser

const cors = require("cors");
app.use(cors({
    domains: '*',
    methods: "*"
}));

const bodyParser = require("body-parser");
app.use(bodyParser.json());





server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});